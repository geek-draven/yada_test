(defproject yada_test "0.0.1-SNAPSHOT"
  :description "FIXME: write description"
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [ring "1.4.0"]
                 [clj-http "2.0.0"]
                 [hiccup "1.0.5"]
                 [com.layerware/hugsql "0.4.7"]
                 [org.postgresql/postgresql "9.4.1207"]
                 [cheshire "5.5.0"]
                 [com.cemerick/url "0.1.1"]
                 [org.clojure/data.json "0.2.6"]
                 [org.clojure/data.csv "0.1.3"]
                 [clj-oauth2 "0.2.0"]
                 [aleph "0.4.1"]
                 [yada "1.1.45"]
                 [selmer "1.0.4"]
                 [bidi "2.0.12"]
                 [buddy/buddy-hashers "1.2.0"]
                 [buddy/buddy-sign "1.4.0"]
                 [prismatic/schema "1.0.4"]
                 [slingshot "0.12.2"]
                 [org.xerial/sqlite-jdbc "3.16.1"]
                 [proto-repl "0.3.1"]]

  :min-lein-version "2.0.0"
  :javac-options ["-target" "1.6" "-source" "1.6" "-Xlint:-options"]
  :uberjar-name "yada_test.jar"
  :aot [yada_test.core]
  :main yada_test.core)
