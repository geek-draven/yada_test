(ns yada_test.core
  (:require [buddy.hashers :as hs]
            [buddy.sign.jws :as jws]
            [schema.core :as s]
            [bidi.vhosts :refer [vhosts-model]]
            [ring.handler.dump :refer [handle-dump]]
            [yada.yada :as yada]
            [selmer.parser :as selmer]
            [clojure.java.io :as io]
            [yada.resources.classpath-resource :refer [new-classpath-resource]])
 (:gen-class))



(defonce server (atom nil))

(defn trunc [s n]
  (subs s 0 (min (count s) n)))


(def scheme "http")
(def host "localhost:8080")





(def routes
         ["" [["/nuke" (yada/resource  {:id :login
                                         :methods
                                           {:get
                                            {:produces [{:media-type "text/html"
                                                         :language "en"}]
                                             :response (fn [ctx]
                                                         (let [route (selmer/set-resource-path! (io/resource "static"))]
                                                           (selmer/render-file "welcome.html"
                                                              {:ctx ctx})))}}})]


              ["/system.html" (yada/resource {:id :sources
                                                         :methods
                                                         {:get
                                                          {:produces [{:media-type "text/html"
                                                                       :language "en"}]
                                                           :response (fn [ctx]
                                                                       (let [route (selmer/set-resource-path! (io/resource "static"))]
                                                                         (selmer/render-file "success.html"
                                                                            {:ctx ctx})))}}})]
              ["/sources.html" (yada/resource {:id :sources
                                                 :methods
                                                   {:get
                                                    {:produces [{:media-type "text/html"
                                                                 :language "en"}]
                                                     :response (fn [ctx]
                                                                 (let [route (selmer/set-resource-path! (io/resource "static"))]
                                                                   (selmer/render-file "success.html"
                                                                      {:ctx ctx})))}}})]


              ["" (new-classpath-resource "static"
                     {:index-files ["welcome.html"]})]]])




(defn -main []
 (let [listener (yada/listener routes
                  {:port 8098})]
   (reset! server listener)))



(defn devmain []
 (let [listener (yada/listener routes
                  {:port 8080})]
   (reset! server listener)))

(defn reset []
  (when (fn? (:close @server))
    ((:close @server)))
  (-main))
